;; Tic-Tac-Toe Program in LISP
;;
;; Author: Shamarr McKinney-VanBuren


;;Initialize the board
(defun initialize-board ()
  (list 'board 0 0 0 0 0 0 0 0 0))



(defun convert-to-letter (v)
  (cond ((eql v 1) "O")
        ((eql v 10) "X")
        (t " ")))




;;Helper function to print board out consistently
(defun print-row (x y z)
  (format t "~&  ~A | ~A | ~A"
          (convert-to-letter x)
          (convert-to-letter y)
          (convert-to-letter z)))
		  
		  
		  
  
;; This function prints out the board
;; Param: board - a list containing elements of a ttt board in row-major order

(defun print-board (board) ;; Creating board spaces 
  (format t "~%")
  (print-row (nth 1 board) (nth 2 board) (nth 3 board))
  (format t "~& -----------")
  (print-row (nth 4 board) (nth 5 board) (nth 6 board))
  (format t "~& -----------")
  (print-row (nth 7 board) (nth 8 board) (nth 9 board))
  (format t "~%~%"))





;;This function places a move on the board
(defun place-move (player pos board)
  (setf (nth pos board) player)
  board)



;;This defniniton clarifies what counts as a 'three in a row'
(defvar *winningThrees*
  '((1 2 3) (4 5 6) (7 8 9) ; Horizontal three in a row
    (1 4 7) (2 5 8) (3 6 9) ; Vertical three in a row
    (1 5 9) (3 5 7)))       ; Diagonal three in a row


;;This helper function helps clarify what counts as a 'three in a row'
(defun total-triplet (board triplet)
  (+ (nth (first triplet) board)
     (nth (second triplet) board)
     (nth (third triplet) board)))


;;This function 'calculates' all winning threes
(defun total-Sums (board)
  (mapcar #'(lambda (triplet)
              (total-triplet board triplet))
          *winningThrees*))
	
	
	
	
;;This function determines if a move is legal
(defun is-legal(board)
  (format t "~&ENTER YOUR MOVE: ")
  (let ((pos (read)))
    (cond ((not (and (integerp pos)
                     (<= 1 pos 9))) ;;Checking in any positions 1-9
           (format t "~&INCORRECT INPUT!") 
           (is-legal board)) ;;Recursively call 
          ((not (zerop (nth pos board)))
           (format t "~&YOU CANNOT PLACE A MOVE HERE!")
           (is-legal board)) ;;Recursively call
          (t pos))))



;;This function determines if someone has won
(defun you-Won(board)
  (let ((sums (total-Sums board)))
    (or (member (* 3 *player*) sums)
        (member (* 3 *computer*) sums))))



;;This functions controls what happens when board is full
(defun full-board(board)
  (not (member 0 board)))
  
  
  
;;This function is for the computer in picking a random empty position to play 
(defun computer-emptySpace (board)
  (let ((pos (+ 1 (random 9))))
    (if (zerop (nth pos board))
      pos(computer-emptySpace board))))
	  
	
	
;;Finds where there is an empty space on the board
(defun emptySpace (board squares)
  (find-if #'(lambda (pos)
               (zerop (nth pos board)))
           squares))



;;Function that helps the computer decide to try to win or block.
(defun win-or-block (board target-sum)
  (let ((triplet (find-if
                   #'(lambda (trip)
                       (equal (total-triplet board trip) target-sum))
                   *winningThrees*)))
    (when triplet
      (emptySpace board triplet))))
	  
	  
	  
	  
(defun computer-move (board)
  "Placeholder"
  board)




;;Function allows computer to randomly put an X or O in on the board, if there's space
(defun random-Moves (board)
  (list (computer-emptySpace board)
        "Random move"))




;;This small Function allows for a bit of game enhancement! 
(defun get-three-in-row (board)
  (let ((pos (win-or-block board (* 2 *computer*))))
    (and pos (list pos "Try to make three in a row"))))		
	
	
	
	
;;This small Function allows for a bit of game enhancement! 
(defun block-player-win (board)
  (let ((pos (win-or-block board (* 2 *player*))))
    (and pos (list pos "Blocking player wins"))))	




;;Function allows for the computer to guess where the best place to mark their turn is
(defun choose-best-move (board)
  (or (get-three-in-row board)
      (block-player-win board)
      (random-Moves board)))
	  
	  
	  
	  
	  
;;Sets the user's allowable behaviors and actions during a game 
(defun player-move (board)
  (format t "~&Your move: ")
  (let* ((pos (is-legal board))
         (new-board (place-move *player* pos board)))
    (print-board new-board)
    (cond ((you-Won new-board) (format t "~&You win!"))
          ((full-board new-board) (format t "~&Tie game."))
          (t (computer-move new-board)))))





;;Sets the computer player's words and behaviors during a game
(defun computer-move (board) 
  (let* ((best-move (choose-best-move board))
         (pos (first best-move))
         (strategy (second best-move))
         (new-board (place-move *computer* pos board)))
    (format t "~&My move: ~S" pos)
    (format t "~&My strategy: ~S" strategy)
    (print-board new-board)
    (cond ((you-Won new-board) (format t "~&I win!"))
          ((full-board new-board) (format t "~&Tie game."))
          (t (player-move new-board)))))
		  
		  



;;This function kicks-off and begins the game
(defun play ()
  (if (y-or-n-p "Would you like to play first? ")
    (player-move (initialize-board))
    (computer-move (initialize-board))))	 



(play)	