Readme File for my LISP Tic-tac-toe Project 3
Last Updated: Thursday, 4/30/2020 
Author: Shamarr McKinney-VanBuren 
Latest version available: bitbucket.org/shamarrmckinney/mckinneyvanburenlispproject3/src 

== Overview ==

This program uses LISP to simulate a traditional tic-tac-toe game.

It prints a 3x3 board and the game begins allows users to place moves on the board

The program also determines whether a particular user move is legal/ allowed. If not, an error exception is 'thrown'

The game ends when either a user scores 3 in a row, or when there is a stalemate.
 
Source code found in src folder in above bit bucket repository. 
 

=======================================================================